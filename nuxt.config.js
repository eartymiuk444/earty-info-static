export default {
    ssr: false,
    target: 'static',
    head: {
        titleTemplate: '%s - ' + process.env.npm_package_name,
        title: process.env.npm_package_name || '',
        meta: [{
            charset: 'utf-8'
        },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ],
        link: [{
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        }]
    },
    loading: {
        color: '#fff'
    },
    css: [],
    plugins: [
        '~/plugins/context-inject.js',
        '~/plugins/vue-inject.js',
        '~/plugins/axios.js',
        '~/plugins/marked.js',
        '~/plugins/dompurify.js',
        '~/plugins/prismjs.js',

        //api
        '~/plugins/api/site-menu-query-api.js',
        '~/plugins/api/site-menu-command-api.js',
        '~/plugins/api/working-page-query-api.js',
        '~/plugins/api/working-page-command-api.js',
        '~/plugins/api/working-card-query-api.js',
        '~/plugins/api/working-card-command-api.js',
        '~/plugins/api/auth-api.js',
        '~/plugins/api/user-api.js',
        '~/plugins/api/image-query-api.js',
        '~/plugins/api/image-command-api.js',
        '~/plugins/api/attachment-query-api.js',
        '~/plugins/api/attachment-command-api.js'
    ],
    buildModules: [
        '@nuxtjs/vuetify',
    ],
    modules: [
        '@nuxtjs/axios'
    ],
    axios: {
        browserBaseURL: '',
    },
    vuetify: {
        customVariables: ['~/assets/variables.scss'],
        treeShake: true,
        // Should match /constants/solarized*.js
        theme: {
            dark: false,
            themes: {
                light: {
                    primary: '#657b83',
                    accent: '#268bd2',
                    secondary: '#93a1a1',
                    info: '#2aa198',
                    warning: '#b58900',
                    error: '#cb4b16',
                    success: '#859900',
                    anchor: '#268bd2'
                },
                dark: {
                    primary: '#839496',
                    accent: '#268bd2',
                    secondary: '#586e75',
                    info: '#2aa198',
                    warning: '#b58900',
                    error: '#cb4b16',
                    success: '#859900',
                    anchor: '#268bd2'
                }
            }
        },
    },
    build: {
        babel: {
            "plugins": [
                ["prismjs", {
                    "languages": [
                        "markup",
                        "css",
                        "clike",
                        "javascript",
                        "apacheconf",
                        "aspnet",
                        "autohotkey",
                        "awk",
                        "bash",
                        "basic",
                        "batch",
                        "bison",
                        "c",
                        "csharp",
                        "cpp",
                        "clojure",
                        "cmake",
                        "cobol",
                        "css-extras",
                        "csv",
                        "diff",
                        "docker",
                        "excel-formula",
                        "fortran",
                        "git",
                        "go",
                        "go-module",
                        "graphql",
                        "groovy",
                        "http",
                        "ignore",
                        "java",
                        "javadoc",
                        "javadoclike",
                        "javastacktrace",
                        "jsdoc",
                        "js-extras",
                        "json",
                        "json5",
                        "jsonp",
                        "jsstacktrace",
                        "kotlin",
                        "latex",
                        "less",
                        "lisp",
                        "log",
                        "lua",
                        "makefile",
                        "markdown",
                        "matlab",
                        "mongodb",
                        "nginx",
                        "objectivec",
                        "pascal",
                        "perl",
                        "php",
                        "phpdoc",
                        "php-extras",
                        "powershell",
                        "properties",
                        "python",
                        "r",
                        "jsx",
                        "tsx",
                        "regex",
                        "ruby",
                        "sass",
                        "scss",
                        "shell-session",
                        "splunk-spl",
                        "sql",
                        "systemd",
                        "typescript",
                        "uri",
                        "vbnet",
                        "vim",
                        "visual-basic",
                        "wiki",
                        "wolfram",
                        "xml-doc",
                        "yaml"],
                    "plugins": ["line-numbers", "toolbar", "copy-to-clipboard"],
                    "theme": "solarizedlight",
                    "css": true
                }]
            ]
        },
        extend(config, ctx) {}
    }
}
