export const state = () => ({
    isLoading: false,
})

export const mutations = {
    //loading
    on(state) {
        state.isLoading = true
    },
    off(state) {
        state.isLoading = false
    },
}

export const actions = {

}

export const getters = {

}