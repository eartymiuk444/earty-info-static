export const state = () => ({
    dialog: false,
    title: undefined,
    button: undefined,
    message: undefined,
    action: undefined,
})

export const getters = {

}

export const mutations = {
    show(state, payload) {
        state.title = "Alert"
        state.button = "OK"
        state.message = ""

        state.dialog = true
        if (payload.title) {
            state.title = payload.title
        }
        if (payload.button) {
            state.button = payload.button
        }
        if (payload.message) {
            state.message = payload.message
        }
    },
    hide(state, payload) {
        state.dialog = false
    },
    updateDialog(state, payload) {
        state.dialog = payload
    },
}
