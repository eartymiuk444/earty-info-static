export const state = () => ({
    userDetails: undefined
})

export const mutations = {
    userDetailsRefreshed(state, payload) {
        state.userDetails = payload
    },
}

export const actions = {
    async refreshUserDetails(context) {
        await this.$userApi.details()
            .then(response => {
                context.commit('userDetailsRefreshed', response)
                context.commit('alerts/enqueueInfoToast', { message: "user details refreshed" }, { root: true })
            })
            .catch(error => {
                context.commit('alerts/enqueueErrorToast', { message: this.$apiErrorMessage(error) }, { root: true } )
            })
    },
}

export const getters = {
    loggedIn(state) {
        return state.userDetails !== undefined && state.userDetails.authorities.includes('ROLE_USER');
    },
    admin(state) {
        return state.userDetails !== undefined && state.userDetails.authorities.includes('ROLE_ADMIN');
    },
}