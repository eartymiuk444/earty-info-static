export const state = () => ({
    dialog: false,
    title: undefined,
    button: undefined,
    message: undefined,
    action: undefined,
    cancelButton: undefined,
    cancelAction: undefined,
    defaultAction: () => { },
    defaultCancelAction: () => { }
})

export const getters = {

}

export const mutations = {
    show(state, payload) {
        state.title = "Are you sure?"
        state.button = "Confirm"
        state.message = ""
        state.action = state.defaultAction
        state.cancelButton = "Cancel"
        state.cancelAction = state.defaultCancelAction

        state.dialog = true
        if (payload.title) {
            state.title = payload.title
        }
        if (payload.button) {
            state.button = payload.button
        }
        if (payload.message) {
            state.message = payload.message
        }
        if (payload.action) {
            state.action = payload.action
        }
        if (payload.cancelButton) {
            state.cancelButton = payload.cancelButton
        }
        if (payload.cancelAction) {
            state.cancelAction = payload.cancelAction
        }
    },
    hide(state, payload) {
        state.dialog = false
    },
    updateDialog(state, payload) {
        state.dialog = payload
    },
    setDefaultActions(state, payload) {
        if (payload.action) {
            state.defaultAction = payload.action
        }
        if (payload.cancelAction) {
            state.defaultCancelAction = payload.cancelAction
        }
    }
}
