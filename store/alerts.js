export const state = () => ({
    nextAlertKey: 0,
    alertPayloads: {},

    snackbarMessageQueue: [],
    snackbarPayload: "",
})

export const getters = {
    hasNextSnackbarMessage(state, getters) {
        return state.snackbarMessageQueue.length > 0
    }
}

export const mutations = {
    addSuccessAlert(state, payload) {
        state.alertPayloads = {...state.alertPayloads, [state.nextAlertKey]: {...payload, type: "success"}}
        state.nextAlertKey++
    },
    addErrorAlert(state, payload) {
        state.alertPayloads = { ...state.alertPayloads, [state.nextAlertKey]: { ...payload, type: "error" } }
        state.nextAlertKey++
    },
    addInfoAlert(state, payload) {
        state.alertPayloads = { ...state.alertPayloads, [state.nextAlertKey]: { ...payload, type: "info" } }
        state.nextAlertKey++
    },
    dismissAlert(state, payload) {
        let alertPayloads = { ...state.alertPayloads }
        delete alertPayloads[payload.key]
        state.alertPayloads = alertPayloads
    },
    enqueueSuccessToast(state, payload) {
        let shallowCopy = state.snackbarMessageQueue.slice()
        shallowCopy.push({ ...payload, color: "success", timeout: 2000 })
        state.snackbarMessageQueue = shallowCopy
        state.nextAlertKey++
    },
    enqueueErrorToast(state, payload) {
        let shallowCopy = state.snackbarMessageQueue.slice()
        shallowCopy.push({ ...payload, color: "error", timeout: 4000 })
        state.snackbarMessageQueue = shallowCopy
        state.nextAlertKey++
    },
    enqueueInfoToast(state, payload) {
        let shallowCopy = state.snackbarMessageQueue.slice()
        shallowCopy.push({ ...payload, color: "info", timeout: 1000 })
        state.snackbarMessageQueue = shallowCopy
        state.nextAlertKey++
    },
    dequeueMessage(state, payload) {
        let shallowCopy = state.snackbarMessageQueue.slice()
        state.snackbarPayload = shallowCopy.shift()
        state.snackbarMessageQueue = shallowCopy
    }
}
