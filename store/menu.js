import { staticMenu as STATIC_MENU , STATIC_DIRECTORY_NAME, STATIC_DIRECTORY_PATH } from "@/constants/static-menu";

const DYNAMIC_DIRECTORY_NAME = "Structured"
const DYNAMIC_DIRECTORY_PATH = "struct"

export const state = () => ({
    apiMenu: undefined
})

export const getters = {
    dynamicMenuLoaded(state) {
        return state.apiMenu !== undefined
    },
    //TODO EA 1/4/2022 - Look into potentially removing. Only used internally right now after switching off of it to drive
    //  all the directory selection lists.
    directories(state) {
        return state.apiMenu === undefined ? {} : state.apiMenu.allDirectories
    },
    dynamicTreeMenuItem(state, getters) {
        let dynamicFullPath = "/" + DYNAMIC_DIRECTORY_PATH

        let allDirectories = getters.directories
        let parentPaths = ["/"]

        return {
            id: dynamicFullPath,
            name: DYNAMIC_DIRECTORY_NAME,
            fullPath: dynamicFullPath,
            parentPaths: parentPaths,
            class: "SubDirectory",
            isDirectory: true,
            children: state.apiMenu === undefined ? [] :
                treeMenuItemChildren(allDirectories[state.apiMenu.rootDirectoryId], dynamicFullPath, allDirectories, parentPaths, true),
            clickable: true,
        }
    },
    homeTreeMenuItem(state, getters) {
        let homePath = ""
        let homeFullPath = "/" + homePath

        return {
            id: homeFullPath,
            name: "Home",
            fullPath: homeFullPath,
            class: "SubDirectory",
            isDirectory: true,
            parentPaths: [],
            children: [staticTreeMenuItem(), getters.dynamicTreeMenuItem],
            clickable: true,
        }
    },
    treeMenuItemsMap(state, getters) {
        return mapTreeMenuItem(getters.homeTreeMenuItem)
    },
    staticMenuItemMap() {
        return staticMenuItemMap()
    },
    dynamicMenuItemMap(state, getters) {
        let dynamicMap = {}

        if (getters.dynamicMenuLoaded) {
            let dynamicDirectory = state.apiMenu.allDirectories[state.apiMenu.rootDirectoryId]

            let tempRoot = {
                directoryId: "",
                items: [{
                    directoryId: dynamicDirectory.directoryId,
                    items: dynamicDirectory.items,
                    class: "SubDirectory",
                    pathSegment: DYNAMIC_DIRECTORY_PATH,
                    name: DYNAMIC_DIRECTORY_NAME,
                }],
                name: "",
                pathSegment: ""
            }

            dynamicMap = mapMenuItemChildren(tempRoot, "", state.apiMenu.allDirectories )
            Object.values(dynamicMap).forEach((value) => {
                value.isStatic = false
                value.isDynamic = true
            })
        }
        return dynamicMap
    },
    menuItemsMap(state, getters) {
        return { ...getters.staticMenuItemMap, ...getters.dynamicMenuItemMap }
    },
    directoryParentMap(state) {
        let map = { }

        if (state.apiMenu) {
            for (let [key, value] of Object.entries(state.apiMenu.allDirectories)) {
                for (let i = 0; i < value.items.length; i++) {
                    if (value.items[i].directoryId) {
                        map[value.items[i].directoryId] = parseInt(key)
                    }
                }
            }
        }

        return map
    },
    pageParentMap(state) {
        let map = { }

        if (state.apiMenu) {
            for (let [key, value] of Object.entries(state.apiMenu.allDirectories)) {
                for (let i = 0; i < value.items.length; i++) {
                    if (value.items[i].workingPageId) {
                        map[value.items[i].workingPageId] = parseInt(key)
                    }
                }
            }
        }

        return map
    },
}

export const mutations = {
    apiMenuRefreshed(state, payload) {
        state.apiMenu = payload
    },
}

export const actions = {
    async refreshApiDynamicMenu(context) {
        await this.$siteMenuQueryApi.query()
            .then(response => {
                context.commit('apiMenuRefreshed', response)
                context.commit('alerts/enqueueInfoToast', { message: "site menu refreshed" }, { root: true })
            })
            .catch(error => {
                context.commit('alerts/enqueueErrorToast', { message: this.$apiErrorMessage(error) }, { root: true } )
            })
    },
}

function staticMenuItemMap() {
    let staticDirectory = STATIC_MENU.allDirectories[STATIC_MENU.rootDirectoryId]

    let tempRoot = {
        directoryId: "",
        items: [{
            directoryId: staticDirectory.directoryId,
            items: staticDirectory.items,
            class: "SubDirectory",
            pathSegment: STATIC_DIRECTORY_PATH,
            name: STATIC_DIRECTORY_NAME,
        }],
        name: "",
        pathSegment: ""
    }

    let staticMap = mapMenuItemChildren(tempRoot, "", STATIC_MENU.allDirectories)
    Object.values(staticMap).forEach((value) => {
        value.isStatic = true
        value.isDynamic = false
    })
    return staticMap
}

function staticTreeMenuItem() {
    let staticFullPath = "/" + STATIC_DIRECTORY_PATH

    let allDirectories = STATIC_MENU.allDirectories
    let parentPaths = ["/"]

    return {
        id: staticFullPath,
        name: STATIC_DIRECTORY_NAME,
        fullPath: staticFullPath,
        parentPaths: parentPaths,
        class: "SubDirectory",
        isDirectory: true,
        children: treeMenuItemChildren(allDirectories[STATIC_MENU.rootDirectoryId], staticFullPath, allDirectories, parentPaths, false),
    }
}

function treeMenuItemChildren(apiMenuItem, parentFullPath, apiDirectoryMap, parentParentPaths, dynamic) {
    let children = []

    for (let i = 0; i < apiMenuItem.items.length; i++) {
        let childFullPath = parentFullPath + "/" + apiMenuItem.items[i].pathSegment
        let childParentPaths = [...parentParentPaths, parentFullPath]

        let child = {
            id: childFullPath,
            name: apiMenuItem.items[i].name,
            fullPath: childFullPath,
            parentPaths: childParentPaths,
            class: apiMenuItem.items[i].class,
            isDirectory: apiMenuItem.items[i].class.includes("SubDirectory"),
            isPage: apiMenuItem.items[i].class.includes("Page"),
            dynamic: dynamic,
        }

        if (child.class.includes("SubDirectory")) {
            children.push({
                ...child,
                directoryId: apiMenuItem.items[i].directoryId,
                children: treeMenuItemChildren(apiDirectoryMap[apiMenuItem.items[i].directoryId], childFullPath, apiDirectoryMap, childParentPaths, dynamic)
            })
        }
        else {
            children.push({
                ...child,
                workingPageId: apiMenuItem.items[i].workingPageId,
                children: []
            })
        }
    }
    return children
}

function mapMenuItemChildren(apiMenuItem, parentPath, apiDirectoryMap) {
    let map = {}

    for (let i = 0; i < apiMenuItem.items.length; i++) {
        let menuItem = {
            id: parentPath + "/" + apiMenuItem.items[i].pathSegment,
            name: apiMenuItem.items[i].name,
            isDirectory: apiMenuItem.items[i].class.includes("SubDirectory"),
            isPage: apiMenuItem.items[i].class.includes("Page"),
            pathSegment: apiMenuItem.items[i].pathSegment
        }

        if (menuItem.isDirectory) {
            menuItem = { ...menuItem, directoryId: apiMenuItem.items[i].directoryId }
            map[menuItem.id] = menuItem
            map = { ...map, ...mapMenuItemChildren(apiDirectoryMap[apiMenuItem.items[i].directoryId], menuItem.id, apiDirectoryMap) }
        }
        else {
            menuItem = { ...menuItem, pageId: apiMenuItem.items[i].workingPageId }
            map[menuItem.id] = menuItem
        }
    }
    return map
}

function mapTreeMenuItem(menuItem) {
    let menuItemsMap = new Map([[menuItem.id, menuItem]])
    for (let i = 0; i < menuItem.children.length; i++) {
        menuItemsMap = new Map([ ...menuItemsMap, ...mapTreeMenuItem(menuItem.children[i]) ])
    }

    return menuItemsMap
}