import Vue from "vue";
import {DEFAULT_STATIC_PAGE_ID, STATIC_PAGES} from "@/constants/static-pages";

export const state = () => ({
    apiPage: undefined,
    rootTreeMenuItem: undefined,
    currentRequestId: -1,
    outstandingRequests: {},
    lastAppliedRequestId: -1,
})

export const getters = {
    pageLoaded(state) {
      return state.apiPage !== undefined
    },
    treeMenuItem(state) {
        return state.apiPage !== undefined ?
            {
                ...state.rootTreeMenuItem,
                children: pageMenuItemChildren(state.apiPage.allOutlineItems[state.apiPage.rootOutlineItemId],
                    state.rootTreeMenuItem.fullPath, state.apiPage.allOutlineItems, [], state.apiPage.displayItemNumbers)
            } : undefined
    },
    treeMenuItemsMap(state, getters) {
        return state.apiPage !== undefined ? mapTreeMenuItem(getters.treeMenuItem) : { }
    },
    parentOutlineItemsMap(state) {
        let map = { }

        if (state.apiPage) {
            for (let [key, value] of Object.entries(state.apiPage.allOutlineItems)) {
                for (let i = 0; i < value.subItems.length; i++) {
                    map[value.subItems[i].workingCardId] = key
                }
            }
        }

        return map
    },
    outlineItems(state) {
        return state.apiPage ? outlineItemChildren(
            state.apiPage.allOutlineItems[state.apiPage.rootOutlineItemId],
            state.apiPage.allOutlineItems) : []
    }
}

export const mutations = {
    selectStatic(state, payload) {
        state.currentRequestId++
        state.apiPage = STATIC_PAGES.has(payload.path) ? STATIC_PAGES.get(payload.path) :
            STATIC_PAGES.get(DEFAULT_STATIC_PAGE_ID)
        state.rootTreeMenuItem = {
            id: payload.path,
            workingPageId: payload.pageId,
            fullPath: payload.path,
            name: payload.name,
            parentPaths: [],
            isOutlineItem: false,
            isPage: true,
            isDirectory: false,
            clickable: true
        }
        state.lastAppliedRequestId = state.currentRequestId
    },
    select(state, payload) {
        state.apiPage = { ...payload.response, rootOutlineItemId: "" }
        state.rootTreeMenuItem = {
            id: payload.path,
            workingPageId: payload.pageId,
            fullPath: payload.path,
            name: payload.name,
            parentPaths: [],
            isOutlineItem: false,
            isPage: true,
            isDirectory: false,
            clickable: true
        }
        state.lastAppliedRequestId = payload.requestId
    },
    clear(state) {
        state.currentRequestId++
        state.apiPage = undefined
        state.rootTreeMenuItem = undefined
        state.lastAppliedRequestId = state.currentRequestId
    },
    requesting(state, payload) {
        state.currentRequestId++
        Vue.set(state.outstandingRequests, state.currentRequestId, {...payload, requestId: state.currentRequestId })
    },
    requestFulfilled(state, payload) {
        Vue.delete(state.outstandingRequests, payload.requestId)
    }
}

export const actions = {
    async retrieveDraft(context, payload) {
        let menuItem = context.rootGetters['menu/menuItemsMap'][payload.path]

        let request = this.$workingPageQueryApi.draft(menuItem.pageId)
        context.commit('requesting', { type: 'retrieveDraft', path: menuItem.id, name: menuItem.name, request: request })
        let requestId = context.state.currentRequestId

        await request.then(response => {
                if (requestId > context.state.lastAppliedRequestId) {
                    context.commit('select', {
                        response: response,
                        requestId: requestId,
                        pageId: menuItem.pageId,
                        path: menuItem.id,
                        name: menuItem.name ,
                    })
                    context.commit('alerts/enqueueSuccessToast', { message: "draft page retrieved" }, { root: true })
                } else {
                    context.commit('alerts/enqueueInfoToast', { message: "throwing out stale page" }, { root: true })
                }
            }).catch(error => {
                context.commit('alerts/enqueueErrorToast', { message: this.$apiErrorMessage(error) }, { root: true })
            }).finally(() => {
                context.commit('requestFulfilled', { requestId: requestId })
            })
    },
    async retrievePublished(context, payload) {
        let menuItem = context.rootGetters['menu/menuItemsMap'][payload.path]

        let request = this.$workingPageQueryApi.published(menuItem.pageId)
        context.commit('requesting', { type: 'retrievePublished', path: menuItem.id, name: menuItem.name, request: request })
        let requestId = context.state.currentRequestId

        await request.then(response => {
            if (requestId > context.state.lastAppliedRequestId) {
                context.commit('select', {
                    response: response,
                    requestId: requestId,
                    pageId: menuItem.pageId,
                    path: menuItem.id,
                    name: menuItem.name ,
                })
                context.commit('alerts/enqueueSuccessToast', { message: "page retrieved" }, { root: true })
            } else {
                context.commit('alerts/enqueueInfoToast', { message: "throwing out stale page" }, { root: true })
            }
        }).catch(error => {
            context.commit('alerts/enqueueErrorToast', { message: this.$apiErrorMessage(error) }, { root: true })
        }).finally(() => {
            context.commit('requestFulfilled', { requestId: requestId })
        })
    },
}

function outlineItemChildren(outlineItem, allOutlineItems) {
    let children = []
    if (outlineItem.subItems) {
        for (let i = 0; i < outlineItem.subItems.length; i++) {
            let childOutlineItem = {
                title: outlineItem.subItems[i].title,
                fragment: outlineItem.subItems[i].fragment,
                workingCardId: outlineItem.subItems[i].workingCardId,
                level: allOutlineItems[outlineItem.subItems[i].workingCardId].level,
                hierarchyNumber: allOutlineItems[outlineItem.subItems[i].workingCardId].hierarchyNumber
            }

            children.push(childOutlineItem)
            children = children.concat(outlineItemChildren(allOutlineItems[outlineItem.subItems[i].workingCardId], allOutlineItems))
        }
    }

    return children
}

function pageMenuItemChildren(apiOutlineItem, parentFullPath, apiOutlineItemMap, parentParentPaths, displayItemNumbers) {
    let children = []

    for (let i = 0; i < apiOutlineItem.subItems.length; i++) {

        let currentSubItem = apiOutlineItem.subItems[i]

        let childParentPaths = [...parentParentPaths, parentFullPath]
        let child = {
            name: displayItemNumbers ? apiOutlineItemMap[currentSubItem.workingCardId].hierarchyNumber + " " + currentSubItem.title : currentSubItem.title,
            parentPaths: childParentPaths,
            isPage: false,
            isDirectory: false,
            isOutlineItem: true,
        }
        let childFullPath
        if (currentSubItem.fragment !== null) {
            childFullPath = parentFullPath.split('#')[0] + "#" + currentSubItem.fragment
            child = {...child,
                id: childFullPath,
                fullPath: childFullPath,
                clickable: true,
            }
        }
        else {
            childFullPath = parentFullPath.split('#')[0] + "#" + currentSubItem.workingCardId
            child = {...child,
                id: childFullPath,
                fullPath: childFullPath,
                clickable: false,
            }
        }

        let apiSubItem = apiOutlineItemMap[currentSubItem.workingCardId]

        if (apiSubItem.subItems) {
            children.push({
                ...child,
                children: pageMenuItemChildren(apiSubItem, childFullPath, apiOutlineItemMap, childParentPaths, displayItemNumbers)
            })
        }
        else {
            children.push({
                ...child,
                children: []
            })
        }
    }
    return children
}

function mapTreeMenuItem(menuItem) {
    let menuItemsMap = { [menuItem.id]: menuItem }
    for (let i = 0; i < menuItem.children.length; i++) {
        menuItemsMap = { ...menuItemsMap, ...mapTreeMenuItem(menuItem.children[i]) }
    }

    return menuItemsMap
}