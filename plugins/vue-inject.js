import Vue from 'vue'

import solarized from '~/constants/solarized.js'
import solarizedLight from '~/constants/solarizedLight.js'
import solarizedDark from '~/constants/solarizedDark.js'

import { getCookie, round, commaFormat, currencyFormat, formatBigNumber, addValuePercent } from "@/plugins/generic-utility";

Vue.prototype.$solarized = solarized
Vue.prototype.$solarizedLight = solarizedLight
Vue.prototype.$solarizedDark = solarizedDark

Vue.prototype.$getCookie = getCookie

Vue.prototype.$round = round
Vue.prototype.$commaFormat = commaFormat
Vue.prototype.$currencyFormat = currencyFormat
Vue.prototype.$formatBigNumber = formatBigNumber
Vue.prototype.$addValuePercent = addValuePercent