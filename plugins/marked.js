import { marked } from 'marked'

export default (context, inject) => {
    context.$marked = marked
    inject('marked', marked)
}