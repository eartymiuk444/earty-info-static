import dompurify from 'dompurify'

export default (context, inject) => {
    context.$dompurify = dompurify
    inject('dompurify', dompurify)
}