import Prism from 'prismjs';

export default (context, inject) => {
    context.$prismjs = Prism
    inject('prismjs', Prism)
}