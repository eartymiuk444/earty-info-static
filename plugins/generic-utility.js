const getCookie = function(key) {
    let name = key + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

const correctPathEndSlash = function(path) {
    let pathLength = path.length
    if (pathLength > 1 && path.slice(-1) === '/') {
        return path.slice(0, pathLength - 1)
    } else {
        return path
    }
}

const normPath = function() {
    return normPathHelper(this.$route.path)
}

const normPathHelper = function(path) {
    return path.replace(/\/+$/, '')
}

const round = function(num) {
    return Math.round(num * 100) / 100
}
const currencyFormat = function(num) {
    return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

const commaFormat = function(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const formatBigNumber = function(bigNumber) {
    // Nine Zeroes for Billions
    let absStringResult = Math.abs(Number(bigNumber)) >= 1.0e+9
        ?
        Math.abs(Number(bigNumber)) / 1.0e+9 + "B"
        // Six Zeroes for Millions
        :
        Math.abs(Number(bigNumber)) >= 1.0e+6
            ?
            Math.abs(Number(bigNumber)) / 1.0e+6 + "M"
            // Three Zeroes for Thousands
            :
            Math.abs(Number(bigNumber)) >= 1.0e+3
                ?
                Math.abs(Number(bigNumber)) / 1.0e+3 + "K"
                :
                Math.abs(Number(bigNumber));
    if (bigNumber > 0) {
        return absStringResult;
    }
    else {
        return "-" + absStringResult;
    }
}

const addValuePercent = function(items) {
    let portfolioValue = 0.0

    for (let i = 0; i < items.length; i++) {
        let shares = parseFloat(items[i].shares.replace(/,/g, ''))
        let price = parseFloat(items[i].price.replace(/[\$,]/g, ''))

        let value = shares * price
        items[i].value = value
        portfolioValue += items[i].value
    }

    for (let i = 0; i < items.length; i++) {
        let percentage = items[i].value / portfolioValue
        items[i].percentage = percentage
    }

    return items
}

const sleep = function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const deepCopy = function deepCopy(obj) {
    return JSON.parse(JSON.stringify(obj))
}

const apiErrorMessage = function apiErrorMessage(apiError) {
    const UNKNOWN_ERROR = "an unknown error occurred"

    let errorMessage

    if (apiError) {
        if (apiError.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            if (apiError.response.data.message) {
                errorMessage = apiError.response.data.message
            }
            else {
                errorMessage = apiError.response.config.url + " resulted in an error; " +
                    "code: " + apiError.response.status +  "; text: " + apiError.response.statusText
            }
        } else if (apiError.request) {
            // The request was made but no response was received
            errorMessage = apiError.config.url + " was requested but no response was received; " +
                "message: " + apiError.message
        } else if (apiError.message) {
            // Something happened in setting up the request that triggered an Error
            errorMessage = apiError.message
        } else {
            errorMessage = UNKNOWN_ERROR
        }
    }
    else {
        errorMessage = UNKNOWN_ERROR
    }
    return errorMessage

}

export { getCookie, correctPathEndSlash, round, currencyFormat, commaFormat, addValuePercent, formatBigNumber, sleep, deepCopy, apiErrorMessage, normPath, normPathHelper }