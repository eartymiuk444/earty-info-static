import {correctPathEndSlash, sleep, deepCopy, apiErrorMessage, normPath, normPathHelper} from "@/plugins/generic-utility";

export default (context, inject) => {

    context.$normPath = normPath
    inject('normPath', normPath)

    context.$normPathHelper = normPathHelper
    inject('normPathHelper', normPathHelper)

    context.$correctPathEndSlash = correctPathEndSlash
    inject('correctPathEndSlash', correctPathEndSlash)

    context.$sleep = sleep
    inject('sleep', sleep)

    context.$deepCopy = deepCopy
    inject('deepCopy', deepCopy)

    context.$apiErrorMessage = apiErrorMessage
    inject('apiErrorMessage', apiErrorMessage)
}