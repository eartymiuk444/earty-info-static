export default (context, inject) => {
    const baseUrl = '/api/attachment/query'

    const attachmentQueryApi = {
        async get(attachmentId) {
            await context.$axios.$get(baseUrl + '/' + attachmentId);
        },
        async getPublished(attachmentId, workingCardId) {
            await context.$axios.$get(baseUrl + '/published/' + attachmentId , { params: { 'workingCardId': workingCardId }});
        },
        async getWithFilename(attachmentId, filename) {
            await context.$axios.$get(baseUrl + '/' + attachmentId + '/' + filename);
        },
        async getPublishedWithFilename(attachmentId, filename, workingCardId) {
            await context.$axios.$get(baseUrl + '/published/' + attachmentId + '/' + filename , { params: { 'workingCardId': workingCardId }});
        }
    }

    context.$attachmentQueryApi = attachmentQueryApi
    inject('attachmentQueryApi', attachmentQueryApi)
}