export default (context, inject) => {
    const baseUrl = '/api/working-page/query'

    const workingPageQueryApi = {
        async draft(workingPageId) {
            return await context.$axios.$get(baseUrl + '/draft', { params: { workingPageId: workingPageId } })
        },
        async published(workingPageId) {
            return await context.$axios.$get(baseUrl + '/published', { params: { workingPageId: workingPageId } })
        },
    }

    context.$workingPageQueryApi = workingPageQueryApi
    inject('workingPageQueryApi', workingPageQueryApi)
}
