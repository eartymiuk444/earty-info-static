export default (context, inject) => {
    const baseUrl = '/api/site-menu/command'

    const siteMenuCommandApi = {
        async create() {
            await context.$axios.$post(baseUrl + '/create')
        },
        async addDirectory(addDirectoryCommand) {
            await context.$axios.$post(baseUrl + '/addDirectory', addDirectoryCommand)
        },
        async moveDirectoryUp(moveDirectoryUpCommand) {
            await context.$axios.$post(baseUrl + '/moveDirectoryUp', moveDirectoryUpCommand)
        },
        async moveDirectoryDown(moveDirectoryDownCommand) {
            await context.$axios.$post(baseUrl + '/moveDirectoryDown', moveDirectoryDownCommand)
        },
        async moveDirectory(moveDirectoryCommand) {
            await context.$axios.$post(baseUrl + '/moveDirectory', moveDirectoryCommand)
        },
        async addPage(addPageCommand) {
            await context.$axios.$post(baseUrl + '/addPage', addPageCommand)
        },
        async movePageUp(movePageUpCommand) {
            await context.$axios.$post(baseUrl + '/movePageUp', movePageUpCommand)
        },
        async movePageDown(movePageDownCommand) {
            await context.$axios.$post(baseUrl + '/movePageDown', movePageDownCommand)
        },
        async movePage(movePageCommand) {
            await context.$axios.$post(baseUrl + '/movePage', movePageCommand)
        },
        async removeDirectory(removeDirectoryCommand) {
            await context.$axios.$delete(baseUrl + '/removeDirectory', {data: removeDirectoryCommand})
        },
        async removePage(removePageCommand) {
            await context.$axios.$delete(baseUrl + '/removePage', { data: removePageCommand })
        },
        async editDirectory(editDirectoryCommand) {
            await context.$axios.$post(baseUrl + '/editDirectory', editDirectoryCommand)
        },
        async changePagePathSegment(changePagePathSegmentCommand) {
            await context.$axios.$post(baseUrl + '/changePagePathSegment', changePagePathSegmentCommand)
        },
    }

    context.$siteMenuCommandApi = siteMenuCommandApi
    inject('siteMenuCommandApi', siteMenuCommandApi)
}
