export default (context, inject) => {
    const baseUrl = '/api/user'

    const userApi = {
        async details() {
            return await context.$axios.$get(baseUrl + '/details')
        }
    }

    context.$userApi = userApi
    inject('userApi', userApi)
}
