export default (context, inject) => {
    const baseUrl = '/api/attachment/command'

    const attachmentCommandApi = {
        async create(attachmentFile, workingCardId) {
            let formData = new FormData()
            formData.append("file", attachmentFile)
            await context.$axios.$post(baseUrl + '/create', formData, { headers: { 'Content-Type': "multipart/form-data" }, params: { 'workingCardId': workingCardId }})
        },
    }

    context.$attachmentCommandApi = attachmentCommandApi
    inject('attachmentCommandApi', attachmentCommandApi)
}