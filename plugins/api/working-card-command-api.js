export default (context, inject) => {
    const baseUrl = '/api/working-card/command'

    const workingCardCommandApi = {
        async changeText(changeTextCommand) {
            await context.$axios.$post(baseUrl + '/changeText', changeTextCommand)
        },
        async removeImage(removeImageCommand) {
            await context.$axios.$delete(baseUrl + '/removeImage', { data: removeImageCommand })
        },
        async removeAttachment(removeAttachmentCommand) {
            await context.$axios.$delete(baseUrl + '/removeAttachment', { data: removeAttachmentCommand })
        }
    }

    context.$workingCardCommandApi = workingCardCommandApi
    inject('workingCardCommandApi', workingCardCommandApi)
}