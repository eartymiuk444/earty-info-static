export default (context, inject) => {
    const baseUrl = '/api/working-card/query'

    const workingCardQueryApi = {
        async draft(workingCardId) {
            return await context.$axios.$get(baseUrl + "/draft", { params: { workingCardId: workingCardId } })
        },
        async published(workingCardId) {
            return await context.$axios.$get(baseUrl + "/published", { params: { workingCardId: workingCardId } })
        },
        async draftCards(workingPageId) {
            return await context.$axios.$get(baseUrl + "/draftCards", { params: { workingPageId: workingPageId } })
        },
        async publishedCards(workingPageId) {
            return await context.$axios.$get(baseUrl + "/publishedCards", { params: { workingPageId: workingPageId } })
        },
    }

    context.$workingCardQueryApi = workingCardQueryApi
    inject('workingCardQueryApi', workingCardQueryApi)
}
