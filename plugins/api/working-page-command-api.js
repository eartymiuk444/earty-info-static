export default (context, inject) => {
    const baseUrl = '/api/working-page/command'

    const workingPageCommandApi = {
        async addOutlineItem(addOutlineItemCommand) {
            await context.$axios.$post(baseUrl + '/addOutlineItem', addOutlineItemCommand)
        },
        async changeOutlineItemFragment(changeOutlineItemFragmentCommand) {
            await context.$axios.$post(baseUrl + '/changeOutlineItemFragment', changeOutlineItemFragmentCommand)
        },
        async removeOutlineItem(removeOutlineItemCommand) {
            await context.$axios.$delete(baseUrl + '/removeOutlineItem', { data: removeOutlineItemCommand })
        },
        async moveOutlineItem(moveOutlineItemCommand) {
            await context.$axios.$post(baseUrl +'/moveOutlineItem', moveOutlineItemCommand)
        },
        async moveOutlineSubItemUp(moveOutlineSubItemUpCommand) {
            await context.$axios.$post(baseUrl + '/moveOutlineSubItemUp', moveOutlineSubItemUpCommand)
        },
        async moveOutlineSubItemDown(moveOutlineSubItemDownCommand) {
            await context.$axios.$post(baseUrl + '/moveOutlineSubItemDown', moveOutlineSubItemDownCommand)
        },
        async discardDraft(discardDraftCommand) {
            await context.$axios.$post(baseUrl + '/discardDraft', discardDraftCommand)
        },
        async publish(publishCommand) {
            await context.$axios.$post(baseUrl + '/publish', publishCommand)
        },
        async changeTitle(changeTitleCommand) {
            await context.$axios.$post(baseUrl + '/changeTitle', changeTitleCommand)
        },
        async changeOutlineItemTitle(changeOutlineItemTitleCommand) {
            await context.$axios.$post(baseUrl + '/changeOutlineItemTitle', changeOutlineItemTitleCommand)
        },
        async toggleDisplayItemNumbers(toggleDisplayItemNumbersCommand) {
            await context.$axios.$post(baseUrl + '/toggleDisplayItemNumbers', toggleDisplayItemNumbersCommand)
        },
    }

    context.$workingPageCommandApi = workingPageCommandApi
    inject('workingPageCommandApi', workingPageCommandApi)
}
