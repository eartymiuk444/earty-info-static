export default (context, inject) => {
    const baseUrl = '/api/image/query'

    const imageQueryApi = {
        async get(imageId) {
            await context.$axios.$get(baseUrl + '/' + imageId);
        },
        async getPublished(imageId, workingCardId) {
            await context.$axios.$get(baseUrl + '/published/' + imageId , { params: { 'workingCardId': workingCardId }});
        },
    }

    context.$imageQueryApi = imageQueryApi
    inject('imageQueryApi', imageQueryApi)
}