export default (context, inject) => {
    const baseUrl = '/api/site-menu/query'

    const siteMenuQueryApi = {
        async query() {
            return await context.$axios.$get(baseUrl)
        }
    }

    context.$siteMenuQueryApi = siteMenuQueryApi
    inject('siteMenuQueryApi', siteMenuQueryApi)
}
