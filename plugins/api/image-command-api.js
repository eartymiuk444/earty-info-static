export default (context, inject) => {
    const baseUrl = '/api/image/command'

    const imageCommandApi = {
        async create(imageFile, workingCardId) {
            let formData = new FormData()
            formData.append("file", imageFile)
            await context.$axios.$post(baseUrl + '/create', formData, { headers: { 'Content-Type': "multipart/form-data" }, params: { 'workingCardId': workingCardId }})
        },
    }

    context.$imageCommandApi = imageCommandApi
    inject('imageCommandApi', imageCommandApi)
}