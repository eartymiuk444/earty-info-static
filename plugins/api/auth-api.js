import { correctPathEndSlash } from "@/plugins/generic-utility";

export default (context, inject) => {
    const baseUrl = '/api/auth'

    const loginUrl = baseUrl + '/login/oauth2/authorization/google'

    const authApi = {
        continueLoginUrl(path, hash) {
            let continuePath = correctPathEndSlash(path) + hash
            return loginUrl + '?continue=' + encodeURIComponent(continuePath)
        },
        async logout() {
            await context.$axios.$post(baseUrl + '/logout');
        }
    }

    context.$authApi = authApi
    inject('authApi', authApi)
}
