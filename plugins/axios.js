export default function ({ $axios, store, $apiErrorMessage }) {

    //It appears as though axios automatically adds the X-XSRF-TOKEN header based on the XSRF-TOKEN cookie.
    //If this wasn't the case we should try to set it on the requests it needs to be set on.
    //The below might not be what we are after however as the helper 'setHeader' might apply globally. Instead
    //we could try to call this in a place that only gets called once;
    // $axios.onRequest(config => {
    //     $axios.setHeader('X-XSRF-TOKEN', getCookie('XSRF-TOKEN'), ['post', 'delete', 'put'])
    // })

    $axios.onRequest(config => {
        //console.log("onRequest")
    })

    $axios.onResponse(response => {
        //console.log("onResponse")
    })

    $axios.onError(error => {
        console.error(error)
        console.error(error.toJSON())
        console.error(error.request)
        console.error($apiErrorMessage(error))

        if (error.response && error.response.status === 401) {
            window.location.assign('/api/auth/login/oauth2/authorization/google?continue=' + window.location.pathname)
        }
    })
}
