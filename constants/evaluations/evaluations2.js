export default Object.freeze({
  BERKSHIRE_TABLE_ITEMS: Object.freeze(
    [{
        name: 'Apple',
        symbol: 'AAPL',
        shares: '907,559,761',
        price: '$127.79',
      },
      {
        name: 'Bank of America Corp',
        symbol: 'BAC',
        shares: '1,032,852,006',
        price: '$35.79'
      },
      {
        name: 'Coca-Cola Co',
        symbol: 'KO',
        shares: '400,000,000',
        price: '$49.90'
      },
      {
        name: 'American Express Company',
        symbol: 'AXP',
        shares: '151,610,700',
        price: '$138.71'
      },
      {
        name: 'Kraft Heinz Co',
        symbol: 'KHC',
        shares: '325,634,818',
        price: '$37.09'
      },
      {
        name: 'Verizon Communications Inc.',
        symbol: 'VZ',
        shares: '146,716,496',
        price: '$55.36'
      },
      {
        name: 'U.S. Bancorp',
        symbol: 'USB',
        shares: '148,766,441',
        price: '$51.16'
      },
      {
        name: 'Moody’s Corporation',
        symbol: 'MCO',
        shares: '24,669,778',
        price: '$282.47'
      },
      {
        name: 'Chevron Corporation',
        symbol: 'CVX',
        shares: '48,498,965',
        price: '$102.05'
      },
      {
        name: 'General Motors Company',
        symbol: 'GM',
        shares: '72,500,000',
        price: '$52.54'
      },
      {
        name: 'Davita Inc',
        symbol: 'DVA',
        shares: '36,095,570',
        price: '$102.85'
      },
      {
        name: 'Bank of New York Mellon Corp',
        symbol: 'BK',
        shares: '74,346,864',
        price: '$43.68'
      },
      {
        name: 'Charter Communications Inc',
        symbol: 'CHTR',
        shares: '5,213,461',
        price: '$619.68'
      },
      {
        name: 'AbbVie Inc',
        symbol: 'ABBV',
        shares: '25,533,082',
        price: '$108.41'
      },
      {
        name: 'Verisign, Inc.',
        symbol: 'VRSN',
        shares: '12,815,613',
        price: '$195.80'
      },
      {
        name: 'Visa Inc',
        symbol: 'V',
        shares: '9,987,460',
        price: '$216.63'
      },
      {
        name: 'Merck & Co., Inc.',
        symbol: 'MRK',
        shares: '28,697,435',
        price: '$72.38'
      },
      {
        name: 'Bristol-Myers Squibb Co',
        symbol: 'BMY',
        shares: '33,336,016',
        price: '$61.63'
      },
      {
        name: 'Wells Fargo & Co',
        symbol: 'WFC',
        shares: '52,423,867',
        price: '$37.09'
      },
      {
        name: 'Liberty Sirius XM Group Series C',
        symbol: 'LSXMK',
        shares: '43,208,291',
        price: '$44.47'
      },
      {
        name: 'Amazon.com, Inc.',
        symbol: 'AMZN',
        shares: '533,300',
        price: '$3,146.14'
      },
      {
        name: 'Snowflake Inc',
        symbol: 'SNOW',
        shares: '6,125,376',
        price: '$272.17'
      },
      {
        name: 'Mastercard Inc',
        symbol: 'MA',
        shares: '4,564,756',
        price: '$362.90'
      },
      {
        name: 'StoneCo Ltd',
        symbol: 'STNE',
        shares: '14,166,748',
        price: '$89.67'
      },
      {
        name: 'Kroger Co',
        symbol: 'KR',
        shares: '33,534,017',
        price: '$32.89'
      },
      {
        name: 'Restoration Hardware Holdings, Inc common stock',
        symbol: 'RH',
        shares: '1,708,348',
        price: '$505.96'
      },
      {
        name: 'Store Capital Corp',
        symbol: 'STOR',
        shares: '24,415,168',
        price: '$33.81'
      },
      {
        name: 'Synchrony Financial',
        symbol: 'SYF',
        shares: '20,128,000',
        price: '$40.00'
      },
      {
        name: 'Liberty Sirius XM Group Series A',
        symbol: 'LSXMA',
        shares: '14,860,360',
        price: '$44.76'
      },
      {
        name: 'Axalta Coating Systems Ltd',
        symbol: 'AXTA',
        shares: '23,420,000',
        price: '$28.24'
      },
      {
        name: 'T-Mobile Us Inc',
        symbol: 'TMUS',
        shares: '5,242,000',
        price: '$123.57'
      },
      {
        name: 'Globe Life Inc',
        symbol: 'GL',
        shares: '6,353,727',
        price: '$95.12'
      },
      {
        name: 'Marsh & McLennan Companies, Inc.',
        symbol: 'MMC',
        shares: '4,267,825',
        price: '$117.46'
      },
      {
        name: 'Teva Pharmaceutical Industries Ltd',
        symbol: 'TEVA',
        shares: '42,789,295',
        price: '$10.92'
      },
      {
        name: 'Liberty Global PLC Class A',
        symbol: 'LBTYA',
        shares: '18,010,000',
        price: '$24.62'
      },
      {
        name: 'Sirius XM Holdings Inc',
        symbol: 'SIRI',
        shares: '50,000,000',
        price: '$6.02'
      },
      {
        name: 'Suncor Energy Inc.',
        symbol: 'SU',
        shares: '13,849,207',
        price: '$20.24'
      },
      {
        name: 'Liberty Global PLC Class C',
        symbol: 'LBTYK',
        shares: '7,346,968',
        price: '$24.44'
      },
      {
        name: 'Biogen Inc',
        symbol: 'BIIB',
        shares: '643,022',
        price: '$273.24'
      },
      {
        name: 'Johnson & Johnson',
        symbol: 'JNJ',
        shares: '327,100',
        price: '$159.32'
      },
      {
        name: 'Procter & Gamble Co',
        symbol: 'PG',
        shares: '315,400',
        price: '$124.29'
      },
      {
        name: 'Liberty Latin America Ltd Class A',
        symbol: 'LILA',
        shares: '2,630,792',
        price: '$12.44'
      },
      {
        name: 'MONDELEZ INTERNATIONAL INC Common Stock',
        symbol: 'MDLZ',
        shares: '578,000',
        price: '$53.66'
      },
      {
        name: 'Liberty Latin America Ltd Class C',
        symbol: 'LILAK',
        shares: '1,284,020',
        price: '$12.48'
      },
      {
        name: 'VANGUARD IX FUN/S&P 500 ETF SHS NEW',
        symbol: 'VOO',
        shares: '43,000',
        price: '$358.12'
      },
      {
        name: 'SPDR S&P 500 ETF Trust',
        symbol: 'SPY',
        shares: '39,400',
        price: '$389.58'
      },
      {
        name: 'United Parcel Service, Inc.',
        symbol: 'UPS',
        shares: '59,400',
        price: '$161.47'
      },
    ]
  ),
  CANNABIS_TABLE_ITEMS: Object.freeze(
    [{
        name: 'Aurora Cannabis',
        cap: 1.53 * 1.0e9,
        priceSales: 4.07,
        revenue: 286000 * 1.0e3,
        incomeLoss: -2820000 * 1.0e3,
        notes: "ACB - Aurora Cannabis Headquarters Edmonton, Canada",
        hq: "Canada"
      },
      {
        name: 'Canopy Growth',
        cap: 9.18 * 1.0e9,
        priceSales: 23.62,
        revenue: 399000 * 1.0e3,
        incomeLoss: -2350000 * 1.0e3,
        notes: "CGC - Headquarters Smith Falls Canada",
        hq: "Canada"
      },
      {
        name: 'Curaleaf Holdings',
        cap: 4.78 * 1.0e9,
        priceSales: 18.33,
        revenue: 221000 * 1.0e3,
        incomeLoss: -52000 * 1.0e3,
        notes: "CURLF - Headquarters Wakefield, MA",
        hq: "United States"
      },
      {
        name: 'GW Pharmaceuticals',
        cap: 3.4 * 1.0e9,
        priceSales: 7.38,
        revenue: 527000 * 1.0e3,
        incomeLoss: -58000 * 1.0e3,
        notes: "GWPRF - Headquarters Cambridge, United Kingdom",
        hq: "United Kingdom"
      },
      {
        name: 'Green Thumb Industries',
        cap: 2.75 * 1.0e9,
        priceSales: 7.36,
        revenue: 216000 * 1.0e3,
        incomeLoss: -59000 * 1.0e3,
        notes: "GTBIF - Headquarters in Chicago, IL.",
        hq: "United States"
      },
      {
        name: 'Cronos Group',
        cap: 2.47 * 1.0e9,
        priceSales: 70.12,
        revenue: 47000 * 1.0e3,
        incomeLoss: -73000 * 1.0e3,
        notes: "CRON - headquarters, Toronto, Canada. ",
        hq: "Canada"
      },
      {
        name: 'Aphria',
        cap: 2.66 * 1.0e9,
        priceSales: 5.18,
        revenue: 542000 * 1.0e3,
        incomeLoss: -86000 * 1.0e3,
        notes: "APHA - Headquarters Leamington, ON Canada",
        hq: "Canada"
      },
      {
        name: '* Cresco Labs',
        cap: 1.30 * 1.0e9,
        priceSales: 4.09,
        revenue: 129000 * 1.0e3,
        incomeLoss: -43000 * 1.0e3,
        notes: "CRLBF - Trades OTC, Don't see much reported to SEC. Headquarters in Chicago, IL. Do see filings on SEDAR",
        hq: "United States"
      },
      {
        name: '* Trulieve Cannabis',
        cap: 2.36 * 1.0e9,
        priceSales: 5.78,
        revenue: 253000 * 1.0e3,
        incomeLoss: 178000 * 1.0e3,
        notes: "TCNNF - Trades OTC, US medical marijuana with headquarters in Florida, didn't see much for financial statements reported to SEC but they do appear to disclose information on there website.",
        hq: "United States"
      },
      {
        name: 'OrganiGram Holdings',
        cap: 329 * 1.0e6,
        priceSales: 4.10,
        revenue: 87000 * 1.0e3,
        incomeLoss: -136000 * 1.0e3,
        notes: "OGI - Headquarters Moncton, Canada",
        hq: "Canada"
      },
      {
        name: 'Tilray',
        cap: 1.31 * 1.0e9,
        priceSales: 4.85,
        revenue: 210000 * 1.0e3,
        incomeLoss: -271000 * 1.0e3,
        notes: "TLRY - Headquarters Nanaimo, Canada",
        hq: "Canada"
      },
    ]
  )

})
