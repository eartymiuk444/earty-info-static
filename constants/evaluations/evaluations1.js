export default Object.freeze({
  BERKSHIRE_TABLE_ITEMS: Object.freeze(
    [{
        name: 'Apple',
        symbol: 'AAPL',
        shares: '250,866,566',
        price: '$462.25',
      },
      {
        name: 'Bank of America Corp',
        symbol: 'BAC',
        shares: '1,032,852,006',
        price: '$25.53'
      },
      {
        name: 'Coca-Cola Co',
        symbol: 'KO',
        shares: '400,000,000',
        price: '$48.42'
      },
      {
        name: 'American Express Company',
        symbol: 'AXP',
        shares: '151,610,700',
        price: '$96.67'
      },
      {
        name: 'Kraft Heinz Co',
        symbol: 'KHC',
        shares: '325,634,818',
        price: '$35.66'
      },
      {
        name: 'Moody’s Corporation',
        symbol: 'MCO',
        shares: '24,669,778',
        price: '$285.49'
      },
      {
        name: 'Wells Fargo & Co',
        symbol: 'WFC',
        shares: '245,566,705',
        price: '$24.04'
      },
      {
        name: 'U.S. Bancorp',
        symbol: 'USB',
        shares: '149,590,275',
        price: '$36.33'
      },
      {
        name: 'Davita Inc',
        symbol: 'DVA',
        shares: '38,095,570',
        price: '$86.54'
      },
      {
        name: 'Charter Communications Inc',
        symbol: 'CHTR',
        shares: '5,213,461',
        price: '$610.69'
      },
      {
        name: 'Bank of New York Mellon Corp',
        symbol: 'BK',
        shares: '74,346,864',
        price: '$36.79'
      },
      {
        name: 'Verisign, Inc.',
        symbol: 'VRSN',
        shares: '12,815,613',
        price: '$206.99'
      },
      {
        name: 'General Motors Company',
        symbol: 'GM',
        shares: '74,681,000',
        price: '$29.84'
      },
      {
        name: 'JPMorgan Chase & Co.',
        symbol: 'JPM',
        shares: '22,208,427',
        price: '$98.32'
      },
      {
        name: 'Visa Inc',
        symbol: 'V',
        shares: '9,987,460',
        price: '$199.01'
      },
      {
        name: 'Amazon.com, Inc.',
        symbol: 'AMZN',
        shares: '533,300',
        price: '$3,312.49'
      },
      {
        name: 'Liberty Sirius XM Group Series C',
        symbol: 'LSXMK',
        shares: '43,208,291',
        price: '$35.33'
      },
      {
        name: 'Mastercard Inc',
        symbol: 'MA',
        shares: '4,564,756',
        price: '$331.00'
      },
      {
        name: 'Costco Wholesale Corporation',
        symbol: 'COST',
        shares: '4,333,363',
        price: '$340.75'
      },
      {
        name: 'Kroger Co',
        symbol: 'KR',
        shares: '21,940,079',
        price: '$36.24'
      },
      {
        name: 'StoneCo Ltd',
        symbol: 'STNE',
        shares: '14,166,748',
        price: '$51.28'
      },
      {
        name: 'Barrick Gold Corp',
        symbol: 'GOLD',
        shares: '20,918,701',
        price: '$30.04'
      },
      {
        name: 'Store Capital Corp',
        symbol: 'STOR',
        shares: '24,415,168',
        price: '$25.38'
      },
      {
        name: 'Axalta Coating Systems Ltd',
        symbol: 'AXTA',
        shares: '24,070,000',
        price: '$23.81'
      },
      {
        name: 'PNC Financial Services Group Inc',
        symbol: 'PNC',
        shares: '5,350,586',
        price: '$107.08'
      },
      {
        name: 'Restoration Hardware Holdings, Inc common stock',
        symbol: 'RH',
        shares: '1,708,348',
        price: '$321.47'
      },
      {
        name: 'Globe Life Inc',
        symbol: 'GL',
        shares: '6,353,727',
        price: '$82.66'
      },
      {
        name: 'Liberty Sirius XM Group Series A',
        symbol: 'LSXMA',
        shares: '14,860,360',
        price: '$35.30'
      },
      {
        name: 'Synchrony Financial',
        symbol: 'SYF',
        shares: '20,128,000',
        price: '$23.98'
      },
      {
        name: 'M&T Bank Corporation',
        symbol: 'MTB',
        shares: '4,536,174',
        price: '$104.50'
      },
      {
        name: 'Teva Pharmaceutical Industries Ltd',
        symbol: 'TEVA',
        shares: '42,789,295',
        price: '$10.48'
      },
      {
        name: 'Liberty Global PLC Class A',
        symbol: 'LBTYA',
        shares: '19,310,000',
        price: '$22.60'
      },
      {
        name: 'Suncor Energy Inc.',
        symbol: 'SU',
        shares: '19,201,525',
        price: '$16.80'
      },
      {
        name: 'Sirius XM Holdings Inc',
        symbol: 'SIRI',
        shares: '50,000,000',
        price: '$5.95'
      },
      {
        name: 'Biogen Inc',
        symbol: 'BIIB',
        shares: '643,022',
        price: '$288.88'
      },
      {
        name: 'Liberty Global PLC Class C',
        symbol: 'LBTYK',
        shares: '7,346,968',
        price: '$21.96'
      },
      {
        name: 'Johnson & Johnson',
        symbol: 'JNJ',
        shares: '327,100',
        price: '$150.09'
      },
      {
        name: 'Procter & Gamble Co',
        symbol: 'PG',
        shares: '315,400',
        price: '$136.51'
      },
      {
        name: 'MONDELEZ INTERNATIONAL INC Common Stock',
        symbol: 'MDLZ',
        shares: '578,000',
        price: '$57.27'
      },
      {
        name: 'Liberty Latin America Ltd Class A',
        symbol: 'LILA',
        shares: '2,630,792',
        price: '$9.57'
      },
      {
        name: 'VANGUARD IX FUN/S&P 500 ETF SHS NEW',
        symbol: 'VOO',
        shares: '43,000',
        price: '$311.16'
      },
      {
        name: 'SPDR S&P 500 ETF Trust',
        symbol: 'SPY',
        shares: '39,400',
        price: '$338.64'
      },
      {
        name: 'Liberty Latin America Ltd Class C',
        symbol: 'LILAK',
        shares: '1,284,020',
        price: '$9.46'
      },
      {
        name: 'United Parcel Service, Inc.',
        symbol: 'UPS',
        shares: '59,400',
        price: '$159.99'
      },
    ]
  ),
  CANNABIS_TABLE_ITEMS: Object.freeze(
    [{
        name: 'Aurora Cannabis',
        cap: 1.15 * 1.0e9,
        priceSales: 4.19,
        revenue: 306 * 1.0e6,
        incomeLoss: -1.4 * 1.0e9,
        notes: "ACB - Aurora Cannabis Headquarters Edmonton, Canada",
        hq: "Canada"
      },
      {
        name: 'Canopy Growth',
        cap: 6.41 * 1.0e9,
        priceSales: 19.40,
        revenue: 419 * 1.0e6,
        incomeLoss: -1.243 * 1.0e9,
        notes: "CGC - Headquarters Smith Falls Canada",
        hq: "Canada"
      },
      {
        name: 'Curaleaf Holdings',
        cap: 4.63 * 1.0e9,
        priceSales: 14.85,
        revenue: 282 * 1.0e6,
        incomeLoss: -72 * 1.0e6,
        notes: "CURLF - Headquarters Wakefield, MA",
        hq: "United States"
      },
      {
        name: 'GW Pharmaceuticals',
        cap: 3.3 * 1.0e9,
        priceSales: 7.64,
        revenue: 442 * 1.0e6,
        incomeLoss: -55 * 1.0e6,
        notes: "GWPRF - Headquarters Cambridge, United Kingdom",
        hq: "United Kingdom"
      },
      {
        name: 'Green Thumb Industries',
        cap: 3.08 * 1.0e9,
        priceSales: 10.29,
        revenue: 291 * 1.0e6,
        incomeLoss: -54 * 1.0e6,
        notes: "GTBIF - Headquarters in Chicago, IL.",
        hq: "United States"
      },
      {
        name: 'Cronos Group',
        cap: 2.00 * 1.0e9,
        priceSales: 61.20,
        revenue: 31.4 * 1.0e6,
        incomeLoss: 635 * 1.0e6,
        notes: "CRON - headquarters, Toronto, Canada. ",
        hq: "Canada"
      },
      {
        name: 'Aphria',
        cap: 1.34 * 1.0e9,
        priceSales: 2.94,
        revenue: 542 * 1.0e6,
        incomeLoss: -85 * 1.0e6,
        notes: "APHA - Headquarters Leamington, ON Canada",
        hq: "Canada"
      },
      {
        name: 'Cresco Labs',
        cap: 1.44 * 1.0e9,
        priceSales: 5.75,
        revenue: 174 * 1.0e6,
        incomeLoss: -44 * 1.0e6,
        notes: "CRLBF - Trades OTC, Don't see much reported to SEC. Headquarters in Chicago, IL. Do see filings on SEDAR",
        hq: "United States"
      },
      {
        name: 'Trulieve Cannabis',
        cap: 2.56 * 1.0e9,
        priceSales: 7.12,
        revenue: 367 * 1.0e6,
        incomeLoss: 126 * 1.0e6,
        notes: "TCNNF - Trades OTC, US medical marijuana with headquarters in Florida, didn't see much for financial statements reported to SEC but they do appear to disclose information on there website.",
        hq: "United States"
      },
      {
        name: 'OrganiGram Holdings',
        cap: 270 * 1.0e6,
        priceSales: 3.78,
        revenue: 83 * 1.0e6,
        incomeLoss: -120 * 1.0e6,
        notes: "OGI - Headquarters Moncton, Canada",
        hq: "Canada"
      },
      {
        name: 'Tilray',
        cap: 9.86 * 1.0e9,
        priceSales: 481.26,
        revenue: 17 * 1.0e6,
        incomeLoss: -102 * 1.0e6,
        notes: "TLRY - Headquarters Nanaimo, Canada",
        hq: "Canada"
      },
    ]
  )
})
