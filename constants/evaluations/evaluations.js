export default Object.freeze({

  BERKSHIRE_TABLE_HEADERS: Object.freeze(
    [{
        text: 'Company Name',
        value: 'name'
      },
      {
        text: 'Symbol',
        value: 'symbol'
      },
      {
        text: 'Shares',
        value: 'shares'
      },
      {
        text: 'Market Price',
        value: 'price'
      },
      {
        text: 'Total Value',
        value: 'value'
      },
      {
        text: 'Percentage of Portfolio',
        value: 'percentage'
      },
    ]
  ),

  CANNABIS_TABLE_HEADERS: Object.freeze(
    [{
        text: 'Company Name',
        value: 'name'
      },
      {
        text: 'Market Cap',
        value: 'cap'
      },
      {
        text: 'Price to Sales',
        value: 'priceSales'
      },
      {
        text: 'Revenue',
        value: 'revenue'
      },
      {
        text: 'Income or Loss',
        value: 'incomeLoss'
      },
      {
        text: "Headquarters",
        value: "hq"
      }
    ]
  )
})
