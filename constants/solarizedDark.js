export default Object.freeze({
      BACKGROUND: '#002b36',
      BACKGROUND_HIGHLIGHT: '#073642',
      PRIMARY_CONTENT: '#839496',
      SECONDARY_CONTENT: '#586e75'
})
