export default Object.freeze({
      YELLOW: '#b58900',
      ORANGE: '#cb4b16',
      RED: '#dc322f',
      MAGENTA: '#d33682',
      VIOLET: '#6c71c4',
      BLUE: '#268bd2',
      CYAN: '#2aa198',
      GREEN: '#859900'
})
