export default Object.freeze({
      BACKGROUND: '#fdf6e3',
      BACKGROUND_HIGHLIGHT: '#eee8d5',
      PRIMARY_CONTENT: '#657b83',
      SECONDARY_CONTENT: '#93a1a1'
})
