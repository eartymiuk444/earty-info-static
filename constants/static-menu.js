const STATIC_DIRECTORY_NAME = "Flexible"
const STATIC_DIRECTORY_PATH = "flex"

let fullStaticPath = "/" + STATIC_DIRECTORY_PATH

let staticMenu =
    {
        rootDirectoryId: "/" + STATIC_DIRECTORY_PATH,
        allDirectories: {
            ["/" + STATIC_DIRECTORY_PATH]: {
                directoryId: "/" + STATIC_DIRECTORY_PATH,
                name: "",
                pathSegment: "",
                items: [
                    {
                        directoryId: fullStaticPath + "/business",
                        class: "SubDirectory",
                        pathSegment: "business",
                        name: "Business",
                    }
                ]
            },
            [fullStaticPath + "/business"]: {
                directoryId: fullStaticPath + "/business",
                name: "Business",
                pathSegment: "business",
                items: [
                    {
                        directoryId: fullStaticPath + "/business/thoughts",
                        class: "SubDirectory",
                        pathSegment: "thoughts",
                        name: "Thoughts"
                    },
                    {
                        directoryId: fullStaticPath + "/business/resources",
                        class: "SubDirectory",
                        pathSegment: "resources",
                        name: "Resources"
                    }
                ]
            },
            [fullStaticPath + "/business/thoughts"]: {
                directoryId: fullStaticPath + "/business/thoughts",
                name: "Thoughts",
                pathSegment: "thoughts",
                items: [
                    {
                        workingPageId: fullStaticPath + "/business/thoughts/1",
                        class: "Page",
                        pathSegment: "1",
                        name: "After Reading Buffet Essays"
                    },
                    {
                        workingPageId: fullStaticPath + "/business/thoughts/2",
                        class: "Page",
                        pathSegment: "2",
                        name: "Biotech Companies"
                    }
                ]
            },
            [fullStaticPath + "/business/resources"]: {
                directoryId: fullStaticPath + "/business/resources",
                name: "Resources",
                pathSegment: "resources",
                items: [
                    {
                        directoryId: fullStaticPath + "/business/resources/personal",
                        class: "SubDirectory",
                        pathSegment: "personal",
                        name: "Personal"
                    }
                ]
            },
            [fullStaticPath + "/business/resources/personal"]: {
                directoryId: fullStaticPath + "/business/resources/personal",
                name: "Personal",
                pathSegment: "personal",
                items: [
                    {
                        directoryId: fullStaticPath + "/business/resources/personal/initial-evaluations",
                        class: "SubDirectory",
                        pathSegment: "initial-evaluations",
                        name: "Initial Evaluations"
                    },
                    {
                        directoryId: fullStaticPath + "/business/resources/personal/reevaluations",
                        class: "SubDirectory",
                        pathSegment: "reevaluations",
                        name: "Re-evaluations"
                    },
                    {
                        directoryId: fullStaticPath + "/business/resources/personal/invest-decisions",
                        class: "SubDirectory",
                        pathSegment: "invest-decisions",
                        name: "Investment Decisions"
                    },
                    {
                        directoryId: fullStaticPath + "/business/resources/personal/plan-review",
                        class: "SubDirectory",
                        pathSegment: "plan-review",
                        name: "Plan Review"
                    }
                ]
            },
            [fullStaticPath + "/business/resources/personal/initial-evaluations"]: {
                directoryId: fullStaticPath + "/business/resources/personal/initial-evaluations",
                name: "Initial Evaluations",
                pathSegment: "initial-evaluations",
                items: [
                    {
                        workingPageId: fullStaticPath + "/business/resources/personal/initial-evaluations/evaluation1",
                        class: "Page",
                        pathSegment: "evaluation1",
                        name: "Initial Evaluation - 08/16/2020"
                    },
                    {
                        workingPageId: fullStaticPath + "/business/resources/personal/initial-evaluations/evaluation2",
                        class: "Page",
                        pathSegment: "evaluation2",
                        name: "Initial Evaluation - 03/02/2021"
                    },
                ]
            },
            [fullStaticPath + "/business/resources/personal/reevaluations"]: {
                directoryId: fullStaticPath + "/business/resources/personal/reevaluations",
                name: "Re-evaluations",
                pathSegment: "reevaluations",
                items: [
                    {
                        workingPageId: fullStaticPath + "/business/resources/personal/reevaluations/reevaluation1",
                        class: "Page",
                        pathSegment: "reevaluation1",
                        name: "Planned Re-evaluation - TBD"
                    }
                ]
            },
            [fullStaticPath + "/business/resources/personal/invest-decisions"]: {
                directoryId: fullStaticPath + "/business/resources/personal/invest-decisions",
                name: "Investment Decisions",
                pathSegment: "invest-decisions",
                items: [
                    {
                        workingPageId: fullStaticPath + "/business/resources/personal/invest-decisions/2020",
                        class: "Page",
                        pathSegment: "2020",
                        name: "2020"
                    },
                    {
                        workingPageId: fullStaticPath + "/business/resources/personal/invest-decisions/2021",
                        class: "Page",
                        pathSegment: "2021",
                        name: "2021"
                    }
                ]
            },
            [fullStaticPath + "/business/resources/personal/plan-review"]: {
                directoryId: fullStaticPath + "/business/resources/personal/plan-review",
                name: "Plan Reviews",
                pathSegment: "plan-review",
                items: [
                    {
                        workingPageId: fullStaticPath + "/business/resources/personal/plan-review/plan-review1",
                        class: "Page",
                        pathSegment: "plan-review1",
                        name: "Planned Plan Review - TBD"
                    }
                ]
            }
        }
    }

export { STATIC_DIRECTORY_NAME, STATIC_DIRECTORY_PATH, staticMenu };
