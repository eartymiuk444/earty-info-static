const DEFAULT_STATIC_PAGE_ID = "DEFAULT_STATIC_PAGE_ID"

const STATIC_PAGES = new Map([
    [DEFAULT_STATIC_PAGE_ID, {
        rootOutlineItemId: "",
        allOutlineItems: {
            "": {
                fragment: null,
                title: "",
                subItems: []
            }
        }
    }]
])

export { DEFAULT_STATIC_PAGE_ID, STATIC_PAGES };
